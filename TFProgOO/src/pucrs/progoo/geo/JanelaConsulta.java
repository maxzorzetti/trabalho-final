/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pucrs.progoo.geo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.GeoPosition;

import pucrs.progoo.Linha;
import pucrs.progoo.Parada;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;

import javax.swing.ListSelectionModel;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 *
 * @author Marcelo Cohen
 */
public class JanelaConsulta extends javax.swing.JFrame {

	private static final long serialVersionUID = 1L;
	
	private Collection<Linha> linhas;
	private Collection<Parada> paradas;
	private List<Parada> paradasSelecionadas;
	private List<MyWaypoint> paradasTodasCoordenadas;
	private DefaultListModel<Linha> listModelResultado;
	private DefaultListModel<Linha> listModelTodasLinhas;
	private ConsultaAtual consultaAtual;

    private GerenciadorMapa gerenciador;
    private EventosMouse mouse;
    
    private JPanel painelMapa;
    private JPanel painelInferior;
    private JButton botaoLimpar;
    private JButton botaoConsulta1Menu;
    private JButton botaoConsulta2Menu;
    private JButton botaoConsulta3Menu;
    private JButton botaoConsulta4Menu;    
    private JPanel painelLateral;
    private JLabel labelPainelLateral;
    private JScrollPane scrollPaneLinhas;
    private JList<Linha> listaLinhas;
    private JPanel painelBotoesLaterais;
    private JButton botaoResetar;
    private JButton botaoConsultar;

    /**
     * Creates new form JanelaConsulta
     */
    public JanelaConsulta( Collection<Linha> linhas , Collection<Parada> paradas ) {
    	super();    	
    	setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
    	/*
    	 * 
    	 */
    	
    	this.linhas = linhas;
    	this.paradas = paradas;    	
    	
    	consultaAtual = ConsultaAtual.NO_CONSULTA;
    	
    	listModelResultado = new DefaultListModel<Linha>();
    	
    	//Preenche o listModel da Consulta 1 com todas as linhas.
    	listModelTodasLinhas = new DefaultListModel<Linha>();    	
    	for(Linha linha: linhas){
    		listModelTodasLinhas.addElement(linha);
    	}    	
		
		paradasSelecionadas = new ArrayList<Parada>();
		
		//Cria e preenche um vetor com MyWaypoints de todas as paradas. Usado nas Consultas 2 e 4 para facilitar a escolha de paradas.
		paradasTodasCoordenadas = new ArrayList<MyWaypoint>();		
		for(Parada parada: paradas){
			MyWaypoint waypoint = new MyWaypoint(new Color(60, 60, 60), "", parada.getCoordenada() );
			paradasTodasCoordenadas.add(waypoint);
		}
		
    	/*
    	 * 
    	 */
        GeoPosition poa = new GeoPosition(-30.05, -51.18);
        gerenciador = new GerenciadorMapa(poa, GerenciadorMapa.FonteImagens.VirtualEarth);
        mouse = new EventosMouse();        		
        gerenciador.getMapKit().getMainMap().addMouseListener(mouse);
        gerenciador.getMapKit().getMainMap().addMouseMotionListener(mouse);       

        painelMapa = new JPanel();
        painelMapa.setLayout(new BorderLayout());
        painelMapa.add(gerenciador.getMapKit(), BorderLayout.CENTER);
                
        getContentPane().add(painelMapa, BorderLayout.CENTER);
        
        painelInferior = new JPanel();
        getContentPane().add(painelInferior, BorderLayout.SOUTH);

        botaoLimpar = new JButton("Limpar");
        botaoLimpar.setToolTipText("Remover todas as linhas e paradas do mapa");
        botaoLimpar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		limpar();
        	}
        });
        painelInferior.add(botaoLimpar);

        botaoConsulta1Menu = new JButton("Consulta 1");
        botaoConsulta1Menu.setToolTipText("Exibir todas as paradas e o tra\u00E7ado de uma linha de \u00F4nibus selecionada");
        botaoConsulta1Menu.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		consulta1Menu();
        	}
        });
        painelInferior.add(botaoConsulta1Menu);

        botaoConsulta2Menu = new JButton("Consulta 2");
        botaoConsulta2Menu.setToolTipText("Exibir as linhas de \u00F4nibus que passam em uma determinada parada");
        botaoConsulta2Menu.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		consulta2Menu();
        	}
        });
        painelInferior.add(botaoConsulta2Menu);

        botaoConsulta3Menu = new JButton("Consulta 3");
        botaoConsulta3Menu.setToolTipText("Exibir as linhas de \u00F4nibus que passam pr\u00F3ximas a determinada localiza\u00E7\u00E3o no mapa");
        botaoConsulta3Menu.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		consulta3Menu();
        	}
        });
        painelInferior.add(botaoConsulta3Menu);

        botaoConsulta4Menu = new JButton("Consulta 4");
        botaoConsulta4Menu.setToolTipText("Exibir as linhas de \u00F4nibus que passam em um conjunto de paradas selecionadas");
        botaoConsulta4Menu.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		consulta4Menu();
        	}
        });
        painelInferior.add(botaoConsulta4Menu);

        painelLateral = new JPanel();
        painelLateral.setVisible(false);
        getContentPane().add(painelLateral, BorderLayout.WEST);
        painelLateral.setLayout(new BorderLayout(0, 0));

        scrollPaneLinhas = new JScrollPane();
        painelLateral.add(scrollPaneLinhas);

        listaLinhas = new JList<Linha>();
        listaLinhas.setValueIsAdjusting(true);
        listaLinhas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        scrollPaneLinhas.setViewportView(listaLinhas);

        painelBotoesLaterais = new JPanel();
        painelLateral.add(painelBotoesLaterais, BorderLayout.SOUTH);
        painelBotoesLaterais.setLayout(new BorderLayout(0, 0));


        botaoResetar = new JButton("Reset");
        botaoResetar.setToolTipText("Mostrar todas as paradas no mapa e esvaziar o conjunto de paradas selecionadas");
        painelBotoesLaterais.add(botaoResetar, BorderLayout.NORTH);
        botaoResetar.setVisible(false);
        botaoResetar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		limpar();
        		paradasSelecionadas.clear();
        		listModelResultado.clear();
        		gerenciador.setPontos(paradasTodasCoordenadas);        		
        	}
        });        

        botaoConsultar = new JButton("Consultar");
        botaoConsultar.setToolTipText("Realizar consulta");
        painelBotoesLaterais.add(botaoConsultar, BorderLayout.SOUTH);                
        
        labelPainelLateral = new JLabel("Bug!");
        labelPainelLateral.setHorizontalAlignment(SwingConstants.CENTER);
        painelLateral.add(labelPainelLateral, BorderLayout.NORTH);
        botaoConsultar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		String command = e.getActionCommand();
        		switch(command){
        		case "consulta1": consulta1(); break;
        		case "consulta2": consulta2(); break;
        		case "consulta3": consulta3(); break;
        		case "consulta4": consulta4(); break;
        		default: System.out.println("Bug!");;
        		}
        	}
        });

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
    }

    private void consulta1Menu() {
    	
    	if(consultaAtual != ConsultaAtual.CONSULTA_1){
    		limpar();
    		
    		listaLinhas.setModel(listModelTodasLinhas);
    		botaoConsultar.setActionCommand("consulta1");
    		
    		botaoResetar.setVisible(false);
    		labelPainelLateral.setText("Linhas de \u00F4nibus");
    		
    		consultaAtual = ConsultaAtual.CONSULTA_1;
    		painelLateral.setVisible(true);
    	} else {
    		painelLateral.setVisible(!painelLateral.isVisible());
    	}
    	
    }
    
    private void consulta1() {
    	limpar();
    	
    	Linha linha = listaLinhas.getSelectedValue();

    	if(linha != null){
    		List<MyWaypoint> paradas = new ArrayList<>();
    		
    		for(Parada parada: linha.getParadas() ){
    			paradas.add( new MyWaypoint(Color.BLACK, "", parada.getCoordenada()) );
    		}
    		gerenciador.setPontos(paradas);

    		gerenciador.snapTo(linha.getCoordenadas().get(0));
    		pintarLinha(linha, Color.RED);

    		//this.repaint();
    	} else {
    		notificar("Escolha uma linha primeiro!");
    	}

	}
    
    private void consulta2Menu() {
    	
    	if(consultaAtual != ConsultaAtual.CONSULTA_2){
    		limpar();
    		
    		listModelResultado.clear();
    		listaLinhas.setModel(listModelResultado);
    		
    		botaoResetar.setVisible(true);
    		labelPainelLateral.setText("Resultado da consulta");
    		gerenciador.setPontos(paradasTodasCoordenadas);
    		
    		paradasSelecionadas.clear();
    		
    		botaoConsultar.setActionCommand("consulta2");
    		consultaAtual = ConsultaAtual.CONSULTA_2;
    		
    		painelLateral.setVisible(true);
    	} else {
    		painelLateral.setVisible(!painelLateral.isVisible());
    	}
    	
    }
    
		
    private void consulta2() {	    	
    		
    	if(!paradasSelecionadas.isEmpty()){
    		listModelResultado.clear();
    		limpar();
    		
    		List<MyWaypoint> pontos = new ArrayList<>();
			pontos.add( new MyWaypoint(Color.BLACK, "", paradasSelecionadas.get(0).getCoordenada()) );
			gerenciador.setPontos( pontos );

    		Collection<Linha> linhas = selecionarLinhasQueContemParadas(paradasSelecionadas);
    		
    		if(!linhas.isEmpty()){
    			for(Linha linha: linhas){
    				pintarLinha(linha, randomColor());
    				listModelResultado.addElement(linha);	
    			}
    		paradasSelecionadas.clear();	
    		
    		} else {
    			notificar("Nenhuma linha foi encontrada para esta parada.\nCulpe o DataPoa!");
    		}

    	} else{
    		notificar("Escolha uma parada primeiro!\nUse o bot�o direito do mouse!");
    	}

	}

    private void consulta3Menu() {   
    	
    	if(consultaAtual != ConsultaAtual.CONSULTA_3){
    		limpar();
    		
    		listModelResultado.clear();
    		listaLinhas.setModel(listModelResultado);
    		
    		botaoResetar.setVisible(false);
    		labelPainelLateral.setText("Resultado da consulta");
    		
    		botaoConsultar.setActionCommand("consulta3");
    		consultaAtual = ConsultaAtual.CONSULTA_3;
    		
    		painelLateral.setVisible(true);
    	} else {
    		painelLateral.setVisible(!painelLateral.isVisible());
    	}
    	
    }
	
	private void consulta3() {
		GeoPosition pos = gerenciador.getPosicao();

/*		try{
			if(pos != null){
				List<Linha> linhas = encontrarLinhasProximas(pos);	//Collection*

				if(!linhas.isEmpty()){				
					limpar(false);
					listModelResultado.clear();

					for(Linha linha: linhas){
						pintarLinha(linha, randomColor());
						listModelResultado.addElement(linha);
					}
				}		
			} else {
				throw new Exception ("Escolha uma posi��o primeiro!\nUse o bot�o direito do mouse!");
			}
		}catch(Exception e){
			popup(e.getMessage());
		}*/
		
		if(pos != null){
			List<Linha> linhasProximas = encontrarLinhasProximas(pos);	//Collection*
			
			if(!linhasProximas.isEmpty()){				
				limpar();
				listModelResultado.clear();
				
				for(Linha linha: linhasProximas){
					pintarLinha(linha, randomColor());
					listModelResultado.addElement(linha);
				}
				
			} else {				
				notificar("Nenhuma linha foi encontrada perto do cursor!");
			}			
		} else {
			notificar("Escolha uma posi��o primeiro!\nUse o bot�o direito do mouse!");
		}
	}
	
    private void consulta4Menu() {    	
    	
    	if(consultaAtual != ConsultaAtual.CONSULTA_4){
    		limpar();
    		
    		listModelResultado.clear();
    		listaLinhas.setModel(listModelResultado);
    		
    		gerenciador.setPontos(paradasTodasCoordenadas);
    		
    		botaoResetar.setVisible(true);
    		labelPainelLateral.setText("Resultado da consulta");
    		
    		paradasSelecionadas.clear();
    		
    		botaoConsultar.setActionCommand("consulta4");
    		consultaAtual = ConsultaAtual.CONSULTA_4;
    		
    		painelLateral.setVisible(true);
    	} else {
    		painelLateral.setVisible(!painelLateral.isVisible());
    	}
    		
    }
	
	private void consulta4() {

    	if(!paradasSelecionadas.isEmpty()){
    		listModelResultado.clear();
    		limpar();
    		
    		List<MyWaypoint> pontos = new ArrayList<>();
    		for(Parada parada: paradasSelecionadas){
    			pontos.add( new MyWaypoint(Color.BLACK, "", parada.getCoordenada()) );
    		}
    		gerenciador.setPontos( pontos );

    		Collection<Linha> linhas = selecionarLinhasQueContemParadas(paradasSelecionadas);
    		
    		if(!linhas.isEmpty()){
    			for(Linha linha: linhas){
    				pintarLinha(linha, randomColor());
    				listModelResultado.addElement(linha);	
    			}
    			paradasSelecionadas.clear();
    		} else if(paradasSelecionadas.size() == 1){
    			notificar("Nenhuma linha foi encontrada para esta parada.\nCulpe o DataPoa!");
    			paradasSelecionadas.clear();
    		} else {
    			notificar("Nenhuma linha foi encontrada para esta combina��o de paradas.");
    		}
    	} else {
    		notificar("Escolha uma parada primeiro!\nUse o bot�o direito do mouse!");
    	}
		
	}
	
	private List<Linha> selecionarLinhasQueContemParadas(Collection<Parada> paradas){
		List<Linha> linhas = new ArrayList<Linha>();
		
		/*
		 * Se uma linha n�o cont�m alguma das paradas recebidas,
		 * N�O adiciona ela � lista de resultado.
		 * i.e. Se ela cont�m todas as paradas recebidas,
		 * adiciona ela � lista de resultados
		 */
		
		for(Linha linha: this.linhas){
			boolean contem = true;
			
			for(Parada parada: paradas){
				if(!linha.getParadas().contains(parada)){ //Se a linha atual n�o cont�m a parada atual, quebra o loop e n�o adiciona a linha no resultado.
					contem = false;
					break;
				}
			}
			if(contem)linhas.add(linha);
		}
		
		return linhas;
	}
	
	private void pintarLinha(Linha linha, Color color) {
        Tracado tracado = new Tracado();
        tracado.setCor(color);
        for(GeoPosition coord: linha.getCoordenadas() ){
        	tracado.addPonto(coord);
        }	        
        gerenciador.addTracado(tracado);   
	}
	
	private List<Linha> encontrarLinhasProximas(GeoPosition pos) {//throws Exception{
		List<Linha> linhas = new ArrayList<Linha>();
		
		/*
		 * Se a dist�ncia de QUALQUER coordenada de uma linha at� 
		 * a coordenada recebida for menor que 0.1 km, adiciona ela
		 * � lista de resultados.
		 */
		
		for(Linha linha: this.linhas) {
			for(GeoPosition pos2: linha.getCoordenadas()) {					
				if(AlgoritmosGeograficos.calcDistancia(pos, pos2) < 0.1){		
					linhas.add(linha);
					break; // Depois de adicionar a linha aos resultados, quebra o loop para evitar que ela seja adicionada novamente aos resultados
				}
			}
		}
		return linhas;
		/*if(!linhas.isEmpty()) return linhas;
		else throw new Exception("Nenhuma linha foi encontrada perto do cursor!");*/
	}
	
	private Parada encontrarParadaMaisProxima(GeoPosition pos) throws Exception{		
		
		/*
		 * Se a dist�ncia de uma parada at� a coordenada recebida
		 * for menor que "dist", retorna a parada.
		 * Quanto menor for o valor inicial e o incremento de "dist",
		 * mais precisa � a busca, mas pior � a performance. 
		 */
		
		for(double dist = 0.05; dist <= 0.3; dist+= 0.05){
			
			for(Parada parada: paradas){    				
				if( AlgoritmosGeograficos.calcDistancia(pos, parada.getCoordenada()) < dist){
					return parada;
				}    				
			}
			
		}		
		
		throw new Exception("Nenhuma parada foi encontrada perto do cursor!");
	}
	
    private class EventosMouse extends MouseAdapter {
    	private int lastButton = -1;    	
    	
    	/*
    	 * Dependendo da consulta sendo realizada, o click do
    	 * bot�o direito do mouse realiza a��es diferentes.
    	 */
    	
    	@Override
    	public void mousePressed(MouseEvent e) {
    		JXMapViewer mapa = gerenciador.getMapKit().getMainMap();
    		GeoPosition loc = mapa.convertPointToGeoPosition(e.getPoint());
    		lastButton = e.getButton();

    		if(lastButton==MouseEvent.BUTTON3) {  		
    			try{
    				switch(consultaAtual){
    				case NO_CONSULTA:
    				case CONSULTA_1:
    					// Intencionalmente branco. i.e. o bot�o direito n�o faz nada quando n�o h� consulta atual ou � a Consulta 1.
    					break;
    				case CONSULTA_2:    					
    					// Limpa a lista de paradas selecionadas antes de selecionar uma parada,
    					// garantindo que na Consulta 2 somente uma parada est� selecionada.
    					paradasSelecionadas.clear(); 
    					paradasSelecionadas.add(encontrarParadaMaisProxima(loc));
    					gerenciador.setPosicao(loc);
    					gerenciador.getMapKit().repaint();   
    					break;
    				case CONSULTA_3:	
    					gerenciador.setPosicao(loc);
    					gerenciador.getMapKit().repaint();    		
    					break;
    				case CONSULTA_4:
    					paradasSelecionadas.add(encontrarParadaMaisProxima(loc));
    					gerenciador.setPosicao(loc);
    					gerenciador.getMapKit().repaint();				
    					break;
    				}
    			} catch(Exception exc){
    				notificar(exc.getMessage());
    			}
    		}
    	}    
    } 
    
    private void notificar(String mensagem){
    	JOptionPane.showMessageDialog(this, mensagem);
    }
    
    private void limpar() {    	
		List<MyWaypoint> clear = new ArrayList<MyWaypoint>();		
		gerenciador.setPontos(clear);
    	gerenciador.clear();
    	gerenciador.setPosicao(null);    	
    	this.repaint();    	
    }	
    
    private Color randomColor(){
    	Random rand = new Random();
    	float r = rand.nextFloat() / 2f + 0.3f;
    	float g = rand.nextFloat() / 2f + 0.3f;
    	float b = rand.nextFloat() / 2f + 0.3f;
    	return new Color(r, g, b);
    }
    
    public enum ConsultaAtual {
    	CONSULTA_1, CONSULTA_2, CONSULTA_3, CONSULTA_4, NO_CONSULTA;
    }
}
