package pucrs.progoo;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.jxmapviewer.viewer.GeoPosition;

public class Linha {
	String nome;
	String codigo;
	List<GeoPosition> coordenadas;
	Set<Parada> paradas;
	
	public Linha(String nome, String codigo) {
		this.nome = nome;
		this.codigo = codigo;
		this.coordenadas = new ArrayList<GeoPosition>();
		this.paradas = new LinkedHashSet<Parada>();
	}
	
	public String getNome(){
		return nome;
	}

	public String getCodigo() {
		return codigo;
	}

	public List<GeoPosition> getCoordenadas() {
		return coordenadas;
	}
	
	public Set<Parada> getParadas() {
		return paradas;
	}
	
	public boolean addParada(Parada parada){
		return paradas.add(parada);
	}
	
	public boolean addCoordenada(GeoPosition coordenada){
		return coordenadas.add(coordenada);
	}	
	
	public String toString(){
		return nome + " - " + codigo;
	}
}
