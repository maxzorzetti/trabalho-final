package pucrs.progoo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

import org.jxmapviewer.viewer.GeoPosition;

public class Leitor {
	
	private static String lineSeparator = "\n";

	public Leitor() {
	}
	
	public static HashMap<Integer, Parada> gerarParadas(Path path){
		HashMap<Integer, Parada> paradas = new HashMap<Integer, Parada>();
		Parada parada;
		GeoPosition coordenada;
		double longitude;
		double latitude;
		Integer idParada;

		try( Scanner input = new Scanner(Files.newBufferedReader(path, Charset.forName("utf-8"))) ){
			input.useDelimiter("[;" + lineSeparator + "]");
			input.nextLine(); // Pula o cabe�alho
			while(input.hasNext()){
				
				idParada = Integer.valueOf(input.next());
				String codigo = input.next().replaceAll("\"", "");
				longitude = Double.parseDouble(input.next().replaceAll("," , ".") );
				latitude = Double.parseDouble(input.next().replaceAll("," , ".") );				
				coordenada = new GeoPosition(latitude, longitude);
				input.next(); // Pula o indicador Terminal			
			
				parada = new Parada(codigo, coordenada);
				
				paradas.put(idParada, parada);				
			}			
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}		
		return paradas;
		
	}

	public static HashMap<Integer, Linha> gerarLinhas(Path path) {		
		HashMap<Integer, Linha> linhas = new HashMap<Integer, Linha>();
		Linha linha;
		
		try ( Scanner input = new Scanner(Files.newBufferedReader(path, Charset.forName("utf-8"))) ){
			input.useDelimiter("[;" + lineSeparator + "]");
			input.nextLine(); // Pula cabe�alho
			while(input.hasNext()){
				Integer idLinha = Integer.parseInt(input.next());
				String nome = input.next().replaceAll("\"", "");
				String codigo = input.next().replaceAll("\"", "");
				String tipo = input.next().replaceAll("\"", "");
				
				if(tipo.equals("O")){
					linha = new Linha(nome, codigo);
					linhas.put(idLinha, linha);					
				}				
			}			
			
		} catch ( IOException e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return linhas;
	}

	public static void preencherLinhasComCoordenadas( Map<Integer, Linha> linhas, Path path) {

		try(Scanner input = new Scanner(Files.newBufferedReader(path, Charset.forName("utf-8")))){
			input.useDelimiter("[;" + lineSeparator + "]");
			input.nextLine(); // Pula o cabe�alho

			while(input.hasNext()){
				input.next(); // Pula o id da coordenada

				String latitudeString = input.next();
				latitudeString = latitudeString.replaceAll(",", ".");
				double latitude = Double.parseDouble(latitudeString);

				String longitudeString = input.next();
				longitudeString = longitudeString.replaceAll(",", ".");
				double longitude = Double.parseDouble(longitudeString);

				String idLinhaString = input.next();
				Integer idLinha = Integer.valueOf(idLinhaString);

				Linha linha = linhas.get(idLinha);
				if(linha != null){
					GeoPosition coordenada = new GeoPosition(latitude, longitude);
					linha.addCoordenada(coordenada);
					linhas.put(idLinha, linha);
				} 
			}
		} catch (Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void preencherLinhasComParadas( Map<Integer, Linha> linhas, Map<Integer, Parada> paradas, Path path ){		
		
		try( Scanner input = new Scanner(Files.newBufferedReader(path, Charset.forName("utf-8"))) ){
			input.useDelimiter("[;" + lineSeparator + "]");
			input.nextLine(); // Pula o cabe�alho
			
			while(input.hasNext()){
				Integer linhaid = Integer.parseInt( input.next() );
				Linha linha = linhas.get( linhaid );
				
				Integer paradaid = Integer.parseInt( input.next() );
				Parada parada = paradas.get( paradaid );
				
				if(parada != null){
					linha.addParada(parada);
				}

			}			
			
		} catch (IOException e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void removerLinhasSemParadas( Map<Integer, Linha> linhas){
		Iterator<Linha> it = linhas.values().iterator();
		while(it.hasNext()){
			Linha linha = it.next();
			if(linha.getParadas().isEmpty()) it.remove();
		}
	}
	
}