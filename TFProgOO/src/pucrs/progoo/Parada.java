package pucrs.progoo;

import org.jxmapviewer.viewer.GeoPosition;

public class Parada {
	String codigo;
	GeoPosition coordenada;
	
	public Parada(String codigo, GeoPosition coordenada) {
		this.codigo = codigo;
		this.coordenada = coordenada;
	}

	public String getCodigo() {
		return codigo;
	}

	public GeoPosition getCoordenada() {
		return coordenada;
	}
	
	public String toString(){
		return "Codigo: " + codigo + " Coordenada: " + coordenada + "\n";
	}
}
