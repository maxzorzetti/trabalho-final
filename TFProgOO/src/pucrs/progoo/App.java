/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pucrs.progoo;

import java.io.IOException;

import java.nio.file.Paths;

import java.util.HashMap;

import pucrs.progoo.geo.JanelaConsulta;

/**
 *
 * @author Marcelo Cohen
 */
public class App {
    public static void main(String[] args) throws IOException{               	
    	
        HashMap<Integer, Parada> paradas = Leitor.gerarParadas(Paths.get("paradas.csv"));
        HashMap<Integer, Linha> linhas = Leitor.gerarLinhas(Paths.get("linhas.csv"));        
        Leitor.preencherLinhasComCoordenadas(linhas,  Paths.get("coordenadas.csv"));
        Leitor.preencherLinhasComParadas(linhas, paradas, Paths.get("paradalinha.csv"));
        Leitor.removerLinhasSemParadas(linhas);
        
        
        JanelaConsulta janela = new JanelaConsulta(linhas.values(), paradas.values());
        janela.setVisible(true);
    }
}
